#include <iostream>
template <class T> void swap(T &l, T &r){
    T n(l);
    r = l;
    l = n;
}
// TODO fix this shit
void quicksort(int* l, int* r) {
    if (r - l <= 1)
        return;
    // центральный элемент массива
    int z = *(l + (r - l) / 2);
    // новые указатели
    int* ll = l, *rr = r - 1;
    while (ll <= rr) {
        while (*ll < z) ll++;
        while (*rr > z) rr--;
        if (ll <= rr) {
            swap(*ll, *rr);
            ll++;
            rr--;
        }
    }
    if (l < rr) quicksort(l, rr + 1);
    if (ll < r) quicksort(ll, r);
}

class Linked_List{
    struct Node{
        int data;
        Node *next;
    };
    Node *head;
public:
    Linked_List(){
        head = nullptr;
    }
    ~Linked_List(){
        Node *next = head;
        while (next){
            Node *delete_ = next;
            next = next->next;
            delete delete_;
        }
    }
    void add(Node *n, int data){
        if (n == nullptr)
            n = new Node;
        else
            add(n->next, data);
    }
    void add(int data){
        Node* n = new Node;
        n->data = data;
        n->next = head;
        head = n;
    }
    void reverse_list(Node *head_){
        Node *next = nullptr,  *prev = nullptr;
        for (Node * buf = head_; buf;) {
            next = buf->next;
            buf->next = prev;
            prev = buf;
            buf = next;
        }
        head = prev;
    }

    void reverse_list(){
        reverse_list(head);
    }
    void rev_list(Node *head_) {
        Node *next, *prev = nullptr;
        for (Node *tmp = head_; tmp;) {
            next = tmp->next;
            tmp->next = prev;
            prev = tmp;
            tmp = next;
        }
        head = prev;
    }

    void reverse_lis(){
        rev_list(head);
    }
    void show(Node * n){
        if (n != nullptr){
            std::cout << n->data << "   ";
            show(n->next);
        }
    }
    void show(){
        show(head);
    }
};

int main() {
    std::cout << "Hello, World!" << std::endl;
    Linked_List list_;
    list_.add(1);
    list_.add(2);
    list_.add(3);
    list_.add(4);
    list_.add(5);
    list_.show();
    std::cout << "\n" << "------------------------------" << "\n";
    list_.reverse_lis();
    list_.show();
    return 0;
}
