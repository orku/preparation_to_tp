#include <iostream>

class Map{
    struct Node{
        char key_;
        int value_;
        Node *next_;
        explicit Node(char key = 0, int value = 0, Node * next = nullptr) : key_(key), value_(value),next_(next) {};
    };
    Node* node;
public:
    Map(){
        node = nullptr;
    }
//    ~Map(){
//        for (Node *next; next != nullptr;) {
//            next = node->next_;
//            delete
//        }
//    }
    void add(Node *n, char key, int val){
        if (n == nullptr)
            n = new Node(key, val);
        else
        add(node->next_, key, val);
    }
    void add_pair(char key, int val){
        add(node, key, val);
    }
    int operator[] (const char key) {
        for ( Node* tmp = node; tmp != nullptr;) {
            if (tmp->key_ == key) {
                return tmp->value_;
            }
            tmp = tmp->next_;
        }
        return 0;
    }
    void show_map(Node *node1){
        for (Node *buf = node; buf != nullptr;){
            std::cout << "key: " << buf->key_ << " | value: " << buf->value_ << std::endl;
            show_map(buf->next_);
        }
    }
    void show_map(){
        show_map(node);
    }
};
struct Str{
    char *str;
    size_t size_;
    explicit Str(char* str = nullptr, size_t sz = 0): str(str), size_(sz){};
//    Str& operator = (Str)
};

int romantic_to_arabic(const char *str, int sz){
    Map map;
    map.add_pair('I', 1);
    map.add_pair('V', 5);
    map.add_pair('X', 10);
    map.add_pair('L', 50);
    map.add_pair('C', 100);
    map.add_pair('D', 500);
    map.add_pair('M', 1000);
    map.show_map();
    int res = 0;
    int buf = 0;
    for (size_t i = 0; i != sz; ++i){
        int n = map[str[i]];
        if (n > buf) {
            res += buf;
            buf = n;
        } else if (n < buf){
            res += n - buf;
            buf = 0;
        } else if (n == buf){
            res += n + buf;
            buf = 0;
        }
        return res;
    }

//int romantic_to_arabic(const char* str, size_t sz){
//    Map map;
//    map.add_pair('I', 1);
//    map.add_pair('V', 5);
//    map.add_pair('X', 10);
//    map.add_pair('L', 50);
//    map.add_pair('C', 100);
//    map.add_pair('D', 500);
//    map.add_pair('M', 1000);
//    map.show_map();
//    int res = 0;
//    int buf = 0;
//    for (size_t i = 0; i; ++i){
//        int n = map[str[i]];
//        if (n > buf) {
//            res += buf;
//            buf = n;
//        } else if (n < buf){
//            res += n - buf;
//            buf = 0;
//        } else if (n == buf){
//            res += n + buf;
//            buf = 0;
//        }
//        return res;
//    }

}
int main() {
    std::cout << "Hello, World!" << std::endl;
    const char *str = "IIIIIIIIII";
//    std::cout << strlen(str) << std::endl;
    std::cout << romantic_to_arabic(str, 5) << std::endl;
    return 0;
}
