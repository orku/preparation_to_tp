//#include <iostream>
//
//struct node{
//    int data_;
//    node *left_, *right_;
//    explicit node(int data = 0, node *left = nullptr, node *right = nullptr) : data_(data), left_(left), right_(right) {};
//};
//
//class b_tree{
//    node *root_;
//
//public:
//    b_tree(){
//        root_ = nullptr;
//    }
//
////    void add(node  *tree_node, int& data){
////        if (tree_node == nullptr)
////            tree_node = new node(data);
////        else{
////            if (tree_node->data_ > data) {
////                add(tree_node->left_, data);
////            } else {
////                add(tree_node->right_, data);
////            }
////        }
////    }
//
//    void add(node *&node, int n) {
//        if (node == nullptr) {
//            node = new struct node(n);
//        } else {
//            if (n > node->data_) {
//                add(node->right_, n);
//            } else {
//                add(node->left_, n);
//            }
//        }
//    }
//
//    void add_new(int& data){
//        add(root_, data);
//    }
//
//    void print_tree(node *&node){
//        if (root_ != nullptr){
//            print_tree(node->right_);
//            std::cout << node->data_ << " ";
//            print_tree(node->left_);
//
//        }
//    }
//    void print_tree_(){
//        if (root_ != nullptr)
//            print_tree(root_);
//    }
//
//    int get_data(){
//        return root_->data_;
//    }
//};
//
//int main() {
//    std::cout << "Hello, World!" << std::endl;
//
//    int n1 = 3, n2 = 4, n3 = 9, n4 = 2, n5 = 6, n6 = 8, n7 = 123;
//    b_tree tree1 = b_tree();
//    b_tree tree2 = b_tree();
//
//    tree1.add_new(n1);
//    tree1.add_new(n2);
//    tree1.add_new(n3);
//    tree1.add_new(n4);
//    tree1.add_new(n5);
//    tree1.add_new(n6);
//    tree1.add_new(n7);
//    tree1.print_tree_();
//    std::cout << tree1.get_data() << std::endl;
//
//    tree2.add_new(n1);
//    tree2.add_new(n2);
//    tree2.add_new(n3);
//    tree2.add_new(n4);
//    tree2.add_new(n5);
//    tree2.add_new(n6);
//    tree2.add_new(n7);
//    tree2.print_tree_();
//
//    return 0;
//}


#include <iostream>

struct node {
    node* left;
    node* right;
    int data_;
    node(int n = 0, node*left = nullptr, node* right = nullptr): left(left), right(right), data_(n) {};
};

class bin_tree {
    node* root_;

public:
    bin_tree() {
        root_ = nullptr;
    }

    void add(node *&node, int n) {
        if (node == nullptr) {
            node = new struct node(n);
        } else {
            if (n > node->data_) {
                add(node->right, n);
            } else {
                add(node->left, n);
            }
        }
    }

    void add_item(int n) {
        add(root_, n);
    }

    void show(node *&node) {
        if (node != nullptr) {
            show(node->left);
            std::cout << node->data_ << " ";
            show(node->right);
        }
    }

    void show_tree() {
        show(root_);
    }

    node* get_head() {
        return root_;
    }
};

bool compare_trees(node* root1, node* root2){
    if ((root1 == nullptr && root2 != nullptr) || (root1 != nullptr && root2 == nullptr))
        return false;
    if (root2 == nullptr && root1 == nullptr)
        return true;
    if (root1->data_ != root2->data_)
        return false;
    return compare_trees(root1->right, root2->right) && compare_trees(root1->left, root2->left);
}


int main() {
    bin_tree BN1;
    BN1.add_item(5);
    BN1.add_item(6);
    BN1.add_item(7);
    BN1.add_item(1);
    BN1.show_tree();

    std::cout << std::endl;

    bin_tree BN2;
    BN2.add_item(5);
    BN2.add_item(6);
    BN2.add_item(7);
    BN2.add_item(1);
    BN2.show_tree();

    std::cout << std::endl;

    std::cout << compare_trees(BN1.get_head(), BN2.get_head());

    return 0;
}

