#include <iostream>
#include <utility>
#include <vector>

typedef std::pair <int, int> Segment;

std::vector <int> get_covering_set(std::vector <Segment> segments) {
  std::vector <int> result;

  // fix this function
  for (std::size_t i = 0; i < segments.size(); i++) {
    result.push_back(segments[i].second);
  }

  return result;
}

int main(void) {
  int segments_count;
  std::cin >> segments_count;
  std::vector <Segment> segments(segments_count);
  for (int i = 0; i < segments_count; i++) {
    std::cin >> segments[i].first >> segments[i].second;
  }

  std::vector <int> points = get_covering_set(segments);
  std::cout << points.size() << std::endl;
  for (std::size_t i = 0; i < points.size(); i++) {
    std::cout << points[i] << " ";
  }
  std::cout << std::endl;
}