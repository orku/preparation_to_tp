#include <iostream>
#include <vector>
#include <algorithm>
#include <map>

int main() {
    std::vector<int> din_array = {};
    din_array.reserve(200);
    for (int i = 0; i < 200; ++i) {
        din_array.push_back(std::rand()%200 - 100);
    }
    std::sort(din_array.begin(), din_array.end());
    for (size_t i = 0; i < din_array.size(); ++i) {
        din_array[i] = din_array[i] * din_array[i];
        std::cout << din_array[i] << "  ";
    }

    std::cout << std::endl;

    std::map<size_t, int> hash_table;
    std::hash<int> hasher = {};
    for (size_t i = 0; i < din_array.size(); ++i) {
        hash_table[hasher(din_array[i])] = din_array[i];
    }
    std::vector<int> sqrt = {};
    for (int i = 0; i < hash_table.size(); ++i) {
        std::cout << hash_table.find(hasher(i*i))->second << "  ";
        sqrt.push_back(hash_table.find(hasher(i*i))->second);
    }
    std::cout << std::endl;
    std::cout << hash_table.size() << "      " <<sqrt.size() <<  std::endl;
    return 0;
}
